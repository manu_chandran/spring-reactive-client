# Spring Reactive Client

This project demonstrates the reactive functionality using Spring WebFlux Module and Thymeleaf.The whole application functionality has been split into two modules, the client and the server and this module comprises of the client side functionality.

## Getting Started

Please clone the project using the command - git clone https://manu_chandran@bitbucket.org/manu_chandran/spring-reactive-client.git

To execute the related server side functionality, clone the dependent project - git clone https://manu_chandran@bitbucket.org/manu_chandran/spring-reactive.git

### Prerequisites

Gradle is used for build management for this project. Apart from gradle, the only prerequisite would be to have Java 1.8 or higher version as the application is built using Spring boot.

### Installing

1. Clone the project to the local workspace using the command - git clone https://manu_chandran@bitbucket.org/manu_chandran/spring-reactive-client.git

2. Start the application using the 'Gradle bootRun' (Using Gradle command)
						or
   Build the project using 'Gradle clean build', then navigate to the build/lib folder and 
   execute the command - 'Java -jar SpringReactiveClient-0.0.1-SNAPSHOT.jar'
   
   
 3. Application will be up and running at the url - http://locahost:8081	
 
 4. To execute the application functionality,the server side API application should be up and running.Please refer the readme file for the server side API project - https://manu_chandran@bitbucket.org/manu_chandran/spring-reactive.git 



## Built With

* [Spring Boot](https://spring.io/projects/spring-boot) - Web framework used
* [Spring WebFlux](https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html) - Reactive Framework
* [Thymeleaf](https://docs.mongodb.com/) - Templating Language
* [Gradle](https://docs.gradle.org/current/userguide/userguide.html) - Build Management Tool



## Author

* **Manu Chandran Shyamala** - (https://bitbucket.org/%7Ba8393a87-bbe7-4fbe-8ad8-1abece085926%7D/)


## License



