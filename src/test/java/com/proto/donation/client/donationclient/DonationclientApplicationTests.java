package com.proto.donation.client.donationclient;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.proto.donation.client.dto.DonationDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DonationclientApplicationTests {

	@Test
	public void contextLoads() {
	}

	
	@Test
	public void testDonationCreation() {
		
		WebClient client2 = WebClient.create("http://localhost:8080");
		
		DonationDTO donation = new DonationDTO();
		donation.setDonationDescription("test donation");
		donation.setDonationID("32111");
		donation.setDonationName("My Donationn Client");
		donation.setDonationType("General");
		
//		MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
//		bodyBuilder.part("file", new ClassPathResource("Capture.JPG"));
//		bodyBuilder.part("donation", donation);

		
	

		Path path = Paths.get("C:\\Users\\c_manu.shyamala\\Desktop\\Transaction_Details.PNG");
		byte[] content = null;
		try {
		    content = Files.readAllBytes(path);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		donation.setDonationImage(content);
		
		client2.post().uri("/donations").accept(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromObject(donation))
        .exchange().block();
	}
}
