package com.proto.donation.client.config;

import java.util.Random;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class DonationClientConfig {
	
	@Bean
	public WebClient webClient() {
		return WebClient.create("http://localhost:8080");
	}
	
	@Bean
	public Random random() {
		return new Random();
	}

}
