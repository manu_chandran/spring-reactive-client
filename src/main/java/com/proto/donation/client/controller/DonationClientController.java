package com.proto.donation.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.thymeleaf.spring5.context.webflux.IReactiveDataDriverContextVariable;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;

import com.proto.donation.client.dto.DonationDTO;
import com.proto.donation.client.dto.Transaction;
import com.proto.donation.client.service.DonationClientService;
import com.proto.donation.client.utils.DonationClientHelper;

@Controller
public class DonationClientController {
	
	String response = "";
	
	@Autowired
	private DonationClientService donationClientService;
		
	@Autowired
	private DonationClientHelper donationClientHelper;
	
	@GetMapping("/users")
	public String reactiveUsers(Model model) {
		model.addAttribute("users", donationClientService.fetchAllUsers());
		return "user" ;
	}
	
	@GetMapping("/donations")
	public String fetchAllDonations(Model model) {
		//model.addAttribute("donations", donationClientService.fetchAllDonations());
		return "donation";
	}
	
	
	@GetMapping("/managedonation")
	public String manageDonation(Model model) {
		//model.addAttribute("input", "delete");
		return "managedonation";
	}
	
	

	
	
	@GetMapping("/showcreatedonation")
	public String showCreateDonation(Model model) {
		DonationDTO donationDTO = new DonationDTO();
		model.addAttribute("donationDTO", donationDTO);
		model.addAttribute("input", "create");
		model.addAttribute("title", "Create Donation");
		return "managedonation";
	}
	
	
	@PostMapping(value = "/createdonation",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String createDonation(Model model,@ModelAttribute DonationDTO donationDTO , @RequestPart("file") FilePart multipartFormData) {
		model.addAttribute("input", "create");
		donationClientHelper.populateDonatationImage(multipartFormData,donationDTO);
		model.addAttribute("success", donationClientService.createDonation(donationDTO));
		return "managedonation";
	}
	
	
	@GetMapping("/loadalldonationforupdate")
	public String loadDonationForUpdate(Model model) {

		model.addAttribute("input", "update");
		model.addAttribute("donationDTOList", donationClientService.fetchAllDonations());
		model.addAttribute("title", "Update Donation");	
		return "managedonation";
	}
	
	
	@GetMapping("/loadselecteddonationforupdate")
	public String updateDonation(Model model,@RequestParam String id) {
				
		model.addAttribute("input", "loaddonationforupdate");
		model.addAttribute("donationDTO", donationClientService.fetchDonationById(id));
		model.addAttribute("title", "Update Donation");	
		return "managedonation";
	}
	
	
	@PostMapping("/updatedonation")
	public String updateDonation(Model model,@ModelAttribute DonationDTO donationDTO) {
				
		model.addAttribute("action", "update");
		model.addAttribute("donationDTO", donationClientService.updateDonation(donationDTO));
		return "managedonation";
	}
	
	
	@GetMapping("/loaddonationfordelete")
	public String loadDonationForDelete(Model model) {		
		model.addAttribute("input", "delete");
		model.addAttribute("donationDTOList", donationClientService.fetchAllDonations());
		model.addAttribute("title", "Delete Donation");	
		return "managedonation";
	}
	
	
	@GetMapping("/deletedonation")
	public String deleteDonation(Model model,@RequestParam String donationID) {
			
		model.addAttribute("action", "delete");
		model.addAttribute("success", donationClientService.deleteDonation(donationID));
		return "managedonation";
	}
	
	
	
	@GetMapping("/listdonations")
	public String listDonations(Model model) {
		model.addAttribute("donationDTOList", donationClientService.fetchAllDonations());	
		return "listdonation";
	}
	
	
	@GetMapping("/performtransaction")
	public String performTransaction(Model model,@RequestParam String donationID) {
		model.addAttribute("donationDTO", donationClientService.fetchDonationById(donationID));
		return "checkout";
	}
	
	
	@PostMapping("/transactionconfirmation")
	public String performTransaction(Model model,@ModelAttribute Transaction transaction) {
		donationClientHelper.generateTransactionNumberAndDate(transaction);
		model.addAttribute("transactionDTO",transaction);
		model.addAttribute("transaction", donationClientService.performTransaction(transaction));
		return "confirmation";
	}
	
	@GetMapping("/transactionhistory")
	public String performTransaction(Model model) {
		
        IReactiveDataDriverContextVariable reactiveDataDrivenMode =
                new ReactiveDataDriverContextVariable(donationClientService.fetchTransactionDetails(), 1);
		model.addAttribute("transactionList", reactiveDataDrivenMode);
		return "transactiondetails";
	}
	

}
