package com.proto.donation.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DonationclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(DonationclientApplication.class, args);
	}

}
