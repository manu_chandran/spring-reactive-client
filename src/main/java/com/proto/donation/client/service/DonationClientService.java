package com.proto.donation.client.service;

import com.proto.donation.client.dto.DonationDTO;
import com.proto.donation.client.dto.Transaction;
import com.proto.donation.client.dto.UserDTO;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DonationClientService {
	
	
	public Flux<UserDTO> fetchAllUsers();

	public Flux<DonationDTO> fetchAllDonations();
		
	public Mono<Void> createDonation(DonationDTO donationDTO); 
	
	public Mono<Void> updateDonation(DonationDTO donationDTO); 
	
	public Mono<DonationDTO> fetchDonationById(String id); 
	
	public Mono<Void> deleteDonation(String id); 
	
	public Mono<Void> performTransaction(Transaction transaction); 
	
	public Flux<Transaction> fetchTransactionDetails(); 
	
}
