package com.proto.donation.client.service.impl;

import java.awt.PageAttributes.MediaType;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.proto.donation.client.dto.DonationDTO;
import com.proto.donation.client.dto.Transaction;
import com.proto.donation.client.dto.UserDTO;
import com.proto.donation.client.service.DonationClientService;
import com.proto.donation.client.utils.DonationClientHelper;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class DonationClientServiceImpl implements DonationClientService {
	
	@Autowired
	private WebClient webClient;
	
	@Autowired
	private DonationClientHelper donationClientHelper;

	@Override
	public Flux<UserDTO> fetchAllUsers() {
		return webClient
				.get()
				.uri("/users")
				.retrieve()
				.bodyToFlux(UserDTO.class);
	}

	@Override
	public Flux<DonationDTO> fetchAllDonations() {
		return webClient
				.get()
				.uri("/donations")
				.retrieve()
				.bodyToFlux(DonationDTO.class);
	}
	

	@Override
	public Mono<Void> createDonation(DonationDTO donationDTO) {
		return webClient
				.post().uri("/donations")
				.body(BodyInserters.fromPublisher(Mono.just(donationDTO),DonationDTO.class))
				.retrieve()
				.bodyToMono(Void.class);
	}


	@Override
	public Mono<Void> updateDonation(DonationDTO donationDTO) {
		return webClient
				.put().uri("/donations")
				.body(BodyInserters.fromPublisher(Mono.just(donationDTO),DonationDTO.class))
				.retrieve()
				.bodyToMono(Void.class);
	}

	@Override
	public Mono<DonationDTO> fetchDonationById(String id) {
		return webClient
				.get()
				.uri("/donations/findone/" + id)
				.retrieve()
				.bodyToMono(DonationDTO.class);
	}

	@Override
	public Mono<Void> deleteDonation(String id) {
		return webClient
				.delete()
				.uri("/donations/findone/" + id)
				.retrieve()
				.bodyToMono(Void.class);
	}

	@Override
	public Mono<Void> performTransaction(Transaction transaction) {
		return webClient
				.post()
				.uri("/transactions")
				.body(BodyInserters.fromPublisher(Mono.just(transaction),Transaction.class))
				.retrieve()
				.bodyToMono(Void.class);
	}

	@Override
	public Flux<Transaction> fetchTransactionDetails() {
		return webClient
				.get()
				.uri("/transactions")
				.retrieve()
				.bodyToFlux(Transaction.class);
	}
	

}
