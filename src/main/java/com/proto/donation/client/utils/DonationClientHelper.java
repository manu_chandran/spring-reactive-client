package com.proto.donation.client.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;

import com.proto.donation.client.dto.DonationDTO;
import com.proto.donation.client.dto.Transaction;

@Component
public class DonationClientHelper {	
	 
	@Autowired
	private Random random;
	
	private SimpleDateFormat simpleDateFormat;
	
	@PostConstruct
	public void init() {
		this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
	}
	 
	 public void populateDonatationImage(FilePart filePart,DonationDTO donationDTO) {
		    Path target = Paths.get("").resolve(filePart.filename());
		    try {
		      Files.deleteIfExists(target);
		      File file = Files.createFile(target).toFile();

		      filePart.transferTo(file).subscribe(consumer ->{
		    	  
		      }, errorConsumer ->{
		    	  System.out.println("Error : "+ errorConsumer);
		      }, () -> {
		    	  System.out.println("Completed");
		    	  try {
					donationDTO.setDonationImage(Files.readAllBytes(file.toPath()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      });
	
		    } catch (IOException e) {
		      throw new RuntimeException(e);
		    }
	}
	 
	public void generateTransactionNumberAndDate(Transaction transaction) {
		transaction.setTransactionID(String.valueOf(random.nextInt(500000))) ;
		transaction.setTransactionDate(new Date());
	}
	 
	public String retrieveDateString(Date date) {
		return simpleDateFormat.format(date);
	}
	
}
