package com.proto.donation.client.dto;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.multipart.MultipartFile;

public class DonationDTO {
	
	private String id;
	
	private String donationID;
	
	private String donationName;
	
	private String donationType;
	
	private String donationDescription;
	
	private byte[] donationImage;
	
	private String donationImageBase64;
	
	private MultipartFile  donationFileImage;
	

	public MultipartFile getDonationFileImage() {
		return donationFileImage;
	}

	public void setDonationFileImage(MultipartFile donationFileImage) {
		this.donationFileImage = donationFileImage;
	}

	public String getDonationImageBase64() {
		donationImageBase64 =Base64.encodeBase64String(this.getDonationImage());
		return donationImageBase64;
	}

	public void setDonationImageBase64(String donationImageBase64) {
		this.donationImageBase64 = donationImageBase64;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDonationID() {
		return donationID;
	}

	public void setDonationID(String donationID) {
		this.donationID = donationID;
	}

	public String getDonationName() {
		return donationName;
	}

	public void setDonationName(String donationName) {
		this.donationName = donationName;
	}

	public String getDonationType() {
		return donationType;
	}

	public void setDonationType(String donationType) {
		this.donationType = donationType;
	}

	public String getDonationDescription() {
		return donationDescription;
	}

	public void setDonationDescription(String donationDescription) {
		this.donationDescription = donationDescription;
	}

	public byte[] getDonationImage() {
		return donationImage;
	}

	public void setDonationImage(byte[] donationImage) {
		this.donationImage = donationImage;
	}

	
	
}
