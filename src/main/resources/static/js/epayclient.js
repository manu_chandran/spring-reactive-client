var pleaseWaitDiv = $('<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false role="dialog">\
        <div class="modal-dialog">\
        <div class="modal-content">\
            <div class="modal-header">\
                <h4 class="modal-title">Please wait...</h4>\
            </div>\
		<div class="modal-body" style="margin-left:auto;margin-right:auto;display:block;margin-top:5%;margin-bottom:0%" >\
		<div class = "loader">\
		</div>\
        </div>\
    </div>\
</div>');
 


 function initiateAjaxRequest(requestUrl,methodType,formObj,removeDivArray,responseRenderDiv,responseDivToHide){
	 
	 $.ajax({
	      url: requestUrl,
	      type: methodType,
	      data: serializeFormInstance(formObj),
	      beforeSend: function(jqXHR, settings) {	    	  
	    	   $("div#divLoading").addClass('show');
	      },
	      success: function(response) {
	    	  $("div#divLoading").removeClass('show');
	    	  initiatePreRenderingForSuccessResponse();
	          //Removing any existing search results and no result outputs  	           
	          if(removeDivArray){
	        	  for (var i in removeDivArray) {	        		    
	        		  $(removeDivArray[i]).remove();
	        	  }
	           }	           
	          //append the response to the result div.
	          $(responseRenderDiv).append(response);	        
	      },
	      error: function(xhr, status, error) {
	    	  $("div#divLoading").removeClass('show');
	    	  renderErrorResponse(responseDivToHide);	    	   	    	   
	    	   var errorResponseDiv = $('<div class="modal fade" id="myModal" role="dialog">\
	    			   <div class="modal-dialog">\
	    			     <div class="modal-content">\
	    			       <div class="modal-header">\
	    			         <button type="button" class="close" data-dismiss="modal">&times;</button>\
	    			         <h4 class="modal-title">Error Occured</h4>\
	    			       </div>\
	    			 		<div class="modal-body" id="errorTextDiv"><p><b>'
	    			   		+	'Error Occured while Processing Request' +
	    			       '</b></p></div>\
	    			       <div class="modal-footer">\
	    			         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\
	    			       </div>\
	    			     </div>\
	    			   </div>\
	    			 </div>');	 
	    	   
	    	   errorResponseDiv.modal();
	    	  
	    	   
	       }
	  });
	 
	 
 }
 
 function serializeFormInstance(formObj){
	 if(formObj){
		 return formObj.serialize();
	 }else{
		 return null;
	 }
 }
 
 
 function initiatePreRenderingForSuccessResponse(){
	  $('#errorDiv').hide();
	  pleaseWaitDiv.modal('hide');
}
 
 function retrieveTransactionDetails() {
	 if( !$("#spcode").val() ||  !$("#servicecode").val() || !$("#sptransactionno").val()){
		 $('#errorDiv').html('<p style="color:red"><b>All Input fields are mandatory<b><p>');
		 return ;
	 }else{
		 $('#errorDiv').html('');
	 }
	 var requestUrl = 'transactionsearch';
	 var requestMethod = 'post';
	 var responseRenderDivNam = "#transactionsearchresults";
	 var $form = $('#transactionsearchattrform');
	 var removeDivArray = {}; 
	 removeDivArray[0] = '#transactionsearchresults-inner-div';
	 removeDivArray[1] = '#transactionsearchformdiv';
	 removeDivArray[2] = '#transactionenquirycaptiondiv';


	 var responseRenderDivNamesToHide = {}; 
	 initiateAjaxRequest(requestUrl,requestMethod,$form,removeDivArray,responseRenderDivNam,responseRenderDivNamesToHide);
	 
 }
 
 function redirectToTransactionSearch(){
	 window.location.href = './transactionsearch'; 
 }
 
 function redirectToHomePage(){
	 window.location.href = './home'; 
 }

 function confirmServiceDelivery(){
	 
	 var requestUrl = 'confirmservicedelivery';
	 var requestMethod = 'post';
	 var responseRenderDivNam = "#transactionconfirmationresults";
	 var $form = $('#transactiondetailsform');
	 var removeDivArray = {}; 
	 removeDivArray[0] = '#transactiondetailsformdiv';
	 removeDivArray[1] = '#transactionconfirmationresults-inner-div';
	 removeDivArray[2] = '#transactionenquirycaptiondiv';


	 var responseRenderDivNamesToHide = {}; 
	 initiateAjaxRequest(requestUrl,requestMethod,$form,removeDivArray,responseRenderDivNam,responseRenderDivNamesToHide);
 }
 
 function hideLoadingDiv(){
	  $("div#divLoading").removeClass('show');
 }
 
 
 function submitTokenRequest(){
	 $('#errorDiv').html('');
	 if( !$("#spCode").val() ||  !$("#servCode").val() || !$("#sptrn").val() || !$("#userName").val() || !$("#emailID").val() || !$("#mobileNo").val() || !$("#amount").val()){
		 $('#errorDiv').html('<p style="color:red"><b>All Input fields are mandatory<b><p>');
		 return ;
	 }
	 if(!validateEmail($("#emailID").val())){
		 $('#errorDiv').html('<p style="color:red"><b>Invalid Email Address<b><p>');
		 return;
	 }
	 if(isNaN($("#amount").val())){
		 $('#errorDiv').html('<p style="color:red"><b>Invalid Amount<b><p>');
		 return;
	 }
	 if(isNaN($("#mobileNo").val()) || $("#mobileNo").val().length != 12){
		 $('#errorDiv').html('<p style="color:red"><b>Invalid Mobile No<b><p>');
		 return;
	 }
	 $("div#divLoading").addClass('show');
	 
	 $("#generatetokenform").submit();
	 
 }
 
 function validateEmail(email) {
	  var regpattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return regpattern.test(email);
}
 
 
function deleteDonationEntry(){
	 $("#donationdeleteform").submit();
}

function selectClickedAmountButton(value){
	
	var allButtonCss = { 'background': 'white'};
	
	$("#amount10").css(allButtonCss);
	$("#amount25").css(allButtonCss);
	$("#amount50").css(allButtonCss);
	$("#amount100").css(allButtonCss);
	
    var clickedButtonCss = { 'background': '#78B7F7'};
    $(value).css(clickedButtonCss);
    
    $("#donationAmount").val($(value).val());
}

function validateCheckOutDetails(){
	 $('#errorDivForBilling').html('');
	 $('#errorDivForCard').html('');
	 if( !$("#firstname").val() ||  !$("#lastname").val() || !$("#email").val() || !$("#address").val() || !$("#state").val() || !$("#country").val() || !$("#zip").val()){
		 $('#errorDivForBilling').html('<p style="color:red"><b>All Billing Detail fields are mandatory<b><p>');
		 return ;
	 }
	
	 if( !$("#cardname").val() ||  !$("#cardnumber").val() || !$("#expiration").val() || !$("#cvv").val() ){
		 $('#errorDivForCard').html('<p style="color:red"><b>All Card Detail fields are mandatory<b><p>');
		 return ;
	 }
	 
	 if( $("#cardnumber").val().length != 16 ){
		 $('#errorDivForCard').html('<p style="color:red"><b>Card Number is Invalid<b><p>');
		 return false;
	 }
	 
	 if( $("#expiration").val().length != 4 ){
		 $('#errorDivForCard').html('<p style="color:red"><b>Invalid Expiry Date<b><p>');
		 return false;
	 }
	 
	 if( $("#cvv").val().length != 3 ){
		 $('#errorDivForCard').html('<p style="color:red"><b>Invalid CVV Code<b><p>');
		 return false;
	 }
	 
	 if( !$("#donationAmount").val() ){
		 $('#errorDivForCard').html('<p style="color:red"><b>Donation Amount is mandatory<b><p>');
		 return false;
	 }
	 

	 
 
	 
}